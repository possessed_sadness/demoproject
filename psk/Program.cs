﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.IO;
using System.Threading.Tasks;


namespace psk
{
    public class Program
    {
        //10,0,0,0=>10,255,255,255
        public static List<Urzadzenie> siec = new List<Urzadzenie>();

        public static string Wyswetl_plik()
        {
            #region includy
            StringBuilder builder = new StringBuilder();
            builder.Append("#include <iostream>").AppendLine();
            builder.Append("#include <fstream>").AppendLine();
            builder.Append("#include <string>").AppendLine();
            builder.Append("#include <cassert>").AppendLine().AppendLine();

            builder.Append("#include \"ns3/core-module.h\"").AppendLine()
                  .AppendLine("#include \"ns3/ipv4.h\"")
                  .AppendLine("#include \"ns3/node.h\"")
                  .AppendLine("#include \"ns3/on-off-helper.h\"")
                  .AppendLine("#include \"ns3/onoff-application.h\"")
                  .AppendLine("#include \"ns3/netanim-module.h\"")
                  .AppendLine("#include \"ns3/internet-module.h\"")
                  .AppendLine("#include \"ns3/point-to-point-module.h\"")
                  .AppendLine("#include \"ns3/network-module.h\"")
                  .AppendLine("#include \"ns3/csma-module.h\"")
                  .AppendLine("#include \"ns3/applications-module.h\"")
                  .AppendLine("#include \"ns3/ipv4-static-routing-helper.h\"")
                  .AppendLine("using namespace ns3;\n\n");


            builder.AppendLine("void printRoutingTable (Ptr<Node> node) {\n" +
            "   Ipv4StaticRoutingHelper helper;\n" +
            "   Ptr<Ipv4> stack = node->GetObject<Ipv4>();\n" +
            "   Ptr<Ipv4StaticRouting> staticrouting = helper.GetStaticRouting(stack);\n" +
            "   uint32_t numroutes = staticrouting->GetNRoutes();\n" +
            "   Ipv4RoutingTableEntry entry;\n" +
            "   std::cout << \"Routing table for device: \" << Names::FindName(node) << \"\\n\";\n" +
            "   std::cout << \"Destination\tMask\t\tGateway\t\tIface\\n\";\n" +
            "\n" +
            "   for (uint32_t i = 0; i < numroutes; i++)\n" +
            "   {\n" +
            "       entry = staticrouting->GetRoute(i);\n" +
            "       std::cout << entry.GetDestNetwork() << \"\\t\"\n" +
            "       << entry.GetDestNetworkMask() << \"\\t\"\n" +
            "       << entry.GetGateway() << \"\\t\\t\"\n" +
            "       << entry.GetInterface() << \"\\n\";\n" +
            "   }\n" +
           "    return;\n" +
           " }\n");

            #endregion includy

            #region p2p
            builder.AppendLine().AppendLine("/////////////////////////// main ///////////////////////");
            builder.Append("int main () {\n").AppendLine();
            builder.AppendLine($"NodeContainer c;\n c.Create({Settings.Default.ilosc_routerow}); ");
            builder.Append("\nInternetStackHelper internet;\ninternet.Install(c);").AppendLine();
            builder.Append("\n // Point-to-point links\n");

            siec.ForEach(n =>
            {
                n.Agenci.ForEach(p2p =>
                {
                    if (p2p.Ip_urzadzenia < p2p.Do_urzadzenia)//unikanie powtórzeń krawędzi bowiem występują w obie strony
                        builder.AppendFormat($"NodeContainer {"n" + p2p.Ip_urzadzenia + "n" + p2p.Do_urzadzenia,-6} = NodeContainer(c.Get({p2p.Ip_urzadzenia}), c.Get({p2p.Do_urzadzenia}));").AppendLine();
                });
            });
            builder.AppendLine().AppendLine("// We create the channels first without any IP addressing information");
            builder.AppendLine("PointToPointHelper p2p;")
                               .AppendLine();

            siec.ForEach(n =>
                        {
                            n.Agenci.ForEach(p2p =>
                            {
                                if (p2p.Ip_urzadzenia < p2p.Do_urzadzenia)
                                {
                                    builder.AppendLine($"p2p.SetDeviceAttribute(\"DataRate\", StringValue(\"{p2p.Predkosc}Mbps\"));")
                                              .AppendLine($"p2p.SetChannelAttribute(\"Delay\", StringValue(\"{p2p.Opoznienie}ms\"));")
                                              .AppendLine($"NetDeviceContainer d{p2p.Ip_urzadzenia}d{p2p.Do_urzadzenia} = p2p.Install(n{p2p.Ip_urzadzenia}n{p2p.Do_urzadzenia}); ")
                                              .AppendLine();
                                }
                            });
                        });
            builder.AppendLine();
            #endregion p2p

            #region ipv4


            builder.AppendLine("\n // Later, we add IP addresses.")
                .AppendLine("Ipv4AddressHelper ipv4;\n");
            siec.ForEach(n =>
                        {
                            n.Agenci.ForEach((p2p) =>
                            {
                                if (p2p.Ip_urzadzenia < p2p.Do_urzadzenia)
                                {
                                    builder
                                    .AppendLine($"ipv4.SetBase(\"{string.Join(".", p2p.poczatkowe_ip)}\", \"255.255.255.0\");")
                                    .AppendLine($"Ipv4InterfaceContainer i{p2p.Ip_urzadzenia}i{p2p.Do_urzadzenia} = ipv4.Assign(d{p2p.Ip_urzadzenia}d{p2p.Do_urzadzenia});\n");
                                }
                            });
                        });

            siec.ForEach(n => { builder.AppendLine($"Ptr<Ipv4> ipv4{n.Id} = c.Get({n.Id})->GetObject<Ipv4>();"); });
            #endregion

            #region device
            builder.AppendLine(string.Join("\n", siec.Select(n => $"\n Ptr<CsmaNetDevice> device{n.Id} = CreateObject<CsmaNetDevice>();" +
            $"\n  c.Get({n.Id})->AddDevice(device{n.Id});")));
            #endregion

            #region h1 h2
            builder.AppendLine("\nIpv4StaticRoutingHelper ipv4RoutingHelper;")
             .AppendLine($" // Create static routes from n{Settings.Default.z_urzadzenia} to n{Settings.Default.do_urzadzenia}");

            builder.AppendLine();

            var mm = siec.SelectMany(n => n.Agenci).Where(p2p => p2p.Ip_urzadzenia < p2p.Do_urzadzenia).Select(p => "d" + p.Ip_urzadzenia + "d" + p.Do_urzadzenia).ToArray();

            #endregion

            #region routing 
            builder.AppendLine();

            siec.ForEach(n =>
            {
                List<string> helper = new List<string>();

                builder.AppendLine($"Ptr <Ipv4StaticRouting> staticRouting{n.Id}= ipv4RoutingHelper.GetStaticRouting(ipv4{n.Id}); ");
                if (n.Agenci.Count == 1)
                {
                    var temp = siec[n.Agenci.First().Do_urzadzenia].Agenci.First(a => string.Join(",", a.poczatkowe_ip).Equals(string.Join(",", n.Agenci.First().poczatkowe_ip)));
                    builder.AppendLine($"staticRouting{n.Id}->SetDefaultRoute(Ipv4Address(" +
                        $"\"{string.Join(".", temp.ip_agenta)}\"),1)");
                    //{siec[temp.Ip_urzadzenia].Agenci.FindIndex(a=>a.ip_agenta.SequenceEqual(temp.ip_agenta))+1});");
                }
                else
                {
                    for (int i = 0; i < n.Agenci.Count; i++)
                    {                     
                        siec[n.Agenci[i].Do_urzadzenia].Agenci.Where(a => a.Do_urzadzenia != n.Id).ToList().ForEach(
                            z =>
                            {
                                var temp = helper.Where(x => x.Equals(string.Join(".",z.poczatkowe_ip))).ToList().Count;

                                if (temp == 0)
                                {
                                    builder.AppendLine($"staticRouting{n.Id}->AddNetworkRouteTo(Ipv4Address(\"" +
                                        $"{string.Join(".", z.poczatkowe_ip)}\"),Ipv4Mask(\"/24\"), Ipv4Address(\"" +
                                        $"{string.Join(".", n.Agenci[i].ip_agenta)}\"), {i + 1});");
                                }
                                else
                                {
                                    builder.AppendLine($"staticRouting{n.Id}->AddNetworkRouteTo(Ipv4Address(\"" +
                                        $"{string.Join(".", z.poczatkowe_ip)}\"),Ipv4Mask(\"/24\"), Ipv4Address(\"" +
                                        $"{string.Join(".", n.Agenci[i].ip_agenta)}\"), {i + 1},{temp});");
                                }

                                helper.Add(string.Join(".", z.poczatkowe_ip));
                            });
                    }
                }
                builder.AppendLine();
            });
            builder.AppendLine();
            #endregion



            #region app start
            builder.AppendLine("UdpEchoServerHelper echoServer(9);\n" +
                "ApplicationContainer serverApps =\n" +
                " echoServer.Install(c.Get(3));\n" +
                " serverApps.Start(Seconds(1.0));\n" +
                " serverApps.Stop(Seconds(10.0));\n" +
                "\n" +
                " UdpEchoClientHelper echoClient(i2i3.GetAddress(1), 9);\n" +
                " echoClient.SetAttribute(\"MaxPackets\", UintegerValue(20));\n" +
                " echoClient.SetAttribute(\"Interval\", TimeValue(Seconds(1)));\n" +
                " echoClient.SetAttribute(\"PacketSize\", UintegerValue(1024));\n" +
                " ApplicationContainer clientApps =\n" +
                " echoClient.Install(c.Get(0));\n" +
                " clientApps.Start(Seconds(2.0));\n" +
                " clientApps.Stop(Seconds(10.0));)\n")

                .Append("// Create a packet sink to receive these packets").AppendLine()
                .Append(""
                + "AsciiTraceHelper ascii;\n"
                + " Ptr<OutputStreamWrapper> stream = ascii.CreateFileStream(\"static-routing-slash32.tr\");\n"
                + "p2p.EnableAsciiAll(stream);\n"
                + "csma.EnableAsciiAll(stream);\n"

                + "printRoutingTable(c.Get(0));\n"
                + " printRoutingTable(c.Get(1));\n"
                + " printRoutingTable(c.Get(2));\n"
                + "printRoutingTable(c.Get(3));\n"
                + "AnimationInterface anim(\"animation2.xml\");\n"
                + "\n"
                + " anim.EnablePacketMetadata(true);\n"
                + " anim.SetConstantPosition(c.Get(0), 50, 100);\n"
                + " anim.SetConstantPosition(c.Get(1), 0, 50);\n"
                + "anim.SetConstantPosition(c.Get(2), 100, 50);\n"
                + "anim.SetConstantPosition(c.Get(3), 50, 0);\n"
                + "Simulator::Run();\n"
                + "Simulator::Destroy();\n"
                + "return 0;\n ");

            builder.AppendLine().Append("}");
            return builder.ToString();
            #endregion
        }
        #region main
        static void Main(string[] args)
        {    //creating device
            Random rnd = new Random();
            for (int i = 0; i < Settings.Default.ilosc_routerow; i++)
            {
                siec.Add(new Urzadzenie(i));
            }
            int last_agent_adres = 1;
            //generacja połączeń mie\ędzy urządzeniami
            for (int i = 0; i < siec.Count; i++)
            {
                for (int j = 0; j < rnd.Next(1, siec.Count - 2); j++)//losujemy ilosc agentów dla sieci
                {
                    int[] a = new int[] { siec[i].Id, 0, rnd.Next(1, 1024), rnd.Next(1, 10) };

                    do
                    {
                        a[1] = rnd.Next(0, siec.Count);
                    } while (a[1] == a[0] || siec[i].Agenci.Where(az => az.Do_urzadzenia == a[1]).Count() > 0);

                    siec[a[0]].Agenci.Add(new Agent2(a[0], a[1], a[2], a[3]
                        , new int[] { 10, 1, last_agent_adres, 0 }
                        , new int[] { 10, 1, last_agent_adres, 1 }
                        , siec[a[1]].Agenci.Count));


                    siec[a[1]].Agenci.Add(new Agent2(a[1], a[0], a[2], a[3]
                        , new int[] { 10, 1, last_agent_adres, 0 }
                        , new int[] { 10, 1, last_agent_adres, 2 }
                        , siec[a[0]].Agenci.Count));

                    last_agent_adres++;
                }
            }



            //wyznaczanie tras
            #region funkcja pomocnicza
            ////////////////////////////////////////////////////////////////////////
            void Trackmaker(int curr, int dest, List<int> track)
            {
                track.Add(curr);
                if (curr == dest)
                {
                    siec[track[0]].rounting_table.Add(track);
                    List<int> v = new List<int>(track);
                    v.Reverse();
                    siec[v[0]].rounting_table.Add(v);
                    return;//znalesiono terasę // zapisanie
                }

                foreach (var item in siec[curr].Agenci)
                {
                    if (track.Contains(item.Do_urzadzenia))
                        continue;
                    Trackmaker(item.Do_urzadzenia, dest, new List<int>(track));
                }
            }
            ////////////////////////////////////////////////////////////////////////
            #endregion funkcja pomocnicza

            for (int i = 0; i < siec.Count - 1; i++)
            {
                for (int j = i + 1; j < siec.Count; j++)
                {
                    Trackmaker(i, j, new List<int>());
                }
            }          
            //zpis do pliku psk.cpp
            using (System.IO.StreamWriter file = new System.IO.StreamWriter(Environment.GetFolderPath(Environment.SpecialFolder.Desktop) + "\\test.cpp"))
            {
                file.WriteLine(Wyswetl_plik());
            }
        }
        #endregion
    }
}
