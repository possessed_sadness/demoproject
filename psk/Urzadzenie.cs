﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
/*
 node - coumputer
 apliccation - user symulation
 Channel- communication channel p2p
 Net Device - a Node may be connected to more than one Channel via multiple NetDevices.
 Topology Helpers - assigning IP addresses, etc
     */
namespace psk
{
            // N-?
   public class Urzadzenie
    {
        public List<List<int>> rounting_table { get; set; } = new List<List<int>>();
        public int Id { get; set; }
        public List<Agent2> Agenci { get; set; } = new List<Agent2>();
            
        public Urzadzenie(int _id)
        {
            this.Id = _id;
        }
      
    } 

//*********************************************************************************
                //node -?-?
    public class Agent2
    {
        public int Ip_urzadzenia { get; set; }
        public int Do_urzadzenia { get; set; }
        public int Predkosc { get; set; }
        public int Opoznienie { get; set; }
        public int[] poczatkowe_ip { get; private set; }
        public int[] ip_agenta { get; private set; }
        public int Index_blizniaka { get; set; }

        public Agent2(int ip_urzadzenia, int do_urzadzenia, int p,int o,int[] _poczatkowe_ip,int[] _ip_agenta,int _Index_blizniaka)
        {
            Ip_urzadzenia = ip_urzadzenia;
            Do_urzadzenia = do_urzadzenia;
            Predkosc = p;
            Opoznienie = o;
            poczatkowe_ip = _poczatkowe_ip;
            ip_agenta = _ip_agenta;
            Index_blizniaka = _Index_blizniaka;
        }

    }
}
